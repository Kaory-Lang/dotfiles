function get_git_branch() {
	local gitBranch=$(git branch --show-current 2>/dev/null)
	local branchHead=$(git log --pretty=format:'%h' -n 1 2>/dev/null)

	if [[ $gitBranch != '' ]] && [[ $branchHead != ''  ]]
	then
		echo -e "\e[32m($gitBranch..$branchHead) "
	elif [[ $gitBranch != '' ]] || [[ $branchHead != ''  ]]
	then
		echo -e "\e[32m($gitBranch$branchHead) "
	fi	
}

#PS1="\e[1m\e[94m\w \$(get_git_branch)\e[96m❯ \e[22m\e[39m"
PS1="\e[1m\e[94m\w \e[96m❯ \e[22m\e[39m"

alias ls="ls --color=auto"
alias vi="vim"
alias firefox="/opt/firefox/firefox"
