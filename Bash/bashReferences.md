# References

- [Common Special Characters](https://www.notion.so/Customizing-Your-Bash-Prompt-ef312daad923421c859bda89a5dd1792)
- [Arrow Icon](https://www.tecmint.com/christmassify-linux-terminal-and-shell/)
- [Formatting/Colors](https://misc.flogisoft.com/bash/tip_colors_and_formatting)
